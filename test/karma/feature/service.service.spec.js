'use strict';

describe('module: main, service: ServiceFactory', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  // beforeEach(module('ngHtml2Js'));

  // instantiate service
  var ServiceFactory;
  beforeEach(inject(function (_ServiceFactory_) {
    ServiceFactory = _ServiceFactory_;
  }));

  it('should do something', function () {
    expect(!!ServiceFactory).toBe(true);
  });

});
