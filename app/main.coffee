'use strict'
angular.module('main', [
  'ionic'
  'ngCordova'
  'ui.router'
]).config ($stateProvider, $urlRouterProvider) ->
  # ROUTING with ui.router
  $urlRouterProvider.otherwise '/main'

  $stateProvider.state 'main',
    url: '/main'
    template: '<ion-view view-title="main"></ion-view>'
  